// Soal No.1 (Array to Object)
console.log("Soal No.1 (Array to Object)")
function arrayToObject(multiDimenArray) {
    if (multiDimenArray === undefined || multiDimenArray.length === 0) {
        return ""
    }
    let currentYear = new Date().getFullYear()
    let arrLength = multiDimenArray.length
    for (let counter = 0; counter < arrLength; counter++) {
        let elementInArr = multiDimenArray[counter]
        let personObj = {
            firstName: elementInArr[0],
            lastName: elementInArr[1],
            gender: elementInArr[2],
            age: elementInArr[3] < currentYear ? currentYear - elementInArr[3] : "Invalid Birth Year"
        }
        if (elementInArr.length < 3) {
            personObj.age = "Invalid Birth Year"
        }
        console.log(`${counter + 1}. ${personObj.firstName} ${personObj.lastName}:`, personObj, "\n")
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


// Soal No. 2 (Shopping Time)
console.log("\n")
console.log("Soal No.2 (Shopping Time)")
function shoppingTime(memberId, money) {
    if (memberId === undefined || memberId === '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }

    let brandOnSale = {
        "Sepatu brand Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    let sortedBrand = Object.keys(brandOnSale).sort((a, b) => brandOnSale[a] - brandOnSale[b])

    function thingsBought(brands) {
        let currentMoney = money
        let arrBought = []
        let reversedBrand = brands.reverse()
        for (let i = 0; i < reversedBrand.length; i++) {
            if (currentMoney - brandOnSale[reversedBrand[i]] < 0) {
                continue
            }
            arrBought.push(reversedBrand[i])
            currentMoney -= brandOnSale[reversedBrand[i]]
        }
        return [arrBought, currentMoney]
    }
    const purchasedStuff = thingsBought(sortedBrand)
    let memberShop = {
        memberId: memberId,
        money: money,
        listPurchased: purchasedStuff[0],
        changeMoney: purchasedStuff[1]
    }
    return memberShop
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No.3 (Naik Angkot)
console.log("\n")
console.log("Soal No.3 (Naik Angkot)")
function naikAngkot(listPenumpang) {
    if (listPenumpang.length === 2) {
        let rute = ['A', 'B', 'C', 'D', 'E', 'F']
        var arrObjRoute = []
        for (let i = 0; i < listPenumpang.length; i++) {
            let passengerObj = {
                penumpang: listPenumpang[i][0],
                naikDari: listPenumpang[i][1],
                tujuan: listPenumpang[i][2],
                bayar: getCost(rute, listPenumpang[i][1], listPenumpang[i][2])
            }
            arrObjRoute.push(passengerObj)
        }
    } else {
        return []
    }

    function getCost(route, wasFrom, destination) {
        let cost = 0
        for (let i = route.indexOf(wasFrom); i < route.indexOf(destination); i++) {
            cost += 1
        }
        return cost * 2000
    }
    return arrObjRoute
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]