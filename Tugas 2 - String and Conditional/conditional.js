// If-else
// Reason in using toUpperCase:
// Untuk mencegah case sensitive.
var nama = "junaedi"
var peran = "werewolf"

// Output untuk Input nama = '' dan peran = ''
if (nama === "" && peran === "") {
    console.log("Nama harus diisi!")
} // Output untuk Input nama = 'Ardian' dan peran = ''
else if (nama.toUpperCase() === "ARDIAN" && peran === "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
} // Output untuk Input nama = 'Jane' dan peran 'Penyihir'
else if (nama.toUpperCase() === "JANE" && peran.toUpperCase() === "PENYIHIR") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} // Output untuk Input nama = 'Jenita' dan peran 'Guard'
else if (nama.toUpperCase() === "JENITA" && peran.toUpperCase() === "GUARD") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} // Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
else if (nama.toUpperCase() === "JUNAEDI" && peran.toUpperCase() === "WEREWOLF") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
}

// Switch-case
var hari = 31
var bulan = 11
var tahun = 2020

wrongInputDate31 = (hari) => {
    if (hari > 31) {
        console.log("Wrong Input on Date!")
        return true
    }
}

wrongInputDate30 = (hari) => {
    if (hari > 30) {
        console.log("Wrong Input on Date!")
        return true
    }
}

switch (bulan) {
    case 1:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Januari " + tahun)
        break;
    case 2:
        if (hari >= 29) {
            console.log("Wrong input on hari!")
            break;
        }
        console.log(hari + " Februari " + tahun)
        break;
    case 3:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Maret " + tahun)
        break;
    case 4:
        if (wrongInputDate30(hari)) { break; }
        console.log(hari + " April " + tahun)
        break;
    case 5:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Mei " + tahun)
        break;
    case 6:
        if (wrongInputDate30(hari)) { break; }
        console.log(hari + " Juni " + tahun)
        break;
    case 7:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Juli " + tahun)
        break;
    case 8:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Agustus " + tahun)
        break;
    case 9:
        if (wrongInputDate30(hari)) { break; }
        console.log(hari + " September " + tahun)
        break;
    case 10:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Oktober " + tahun)
        break;
    case 11:
        if (wrongInputDate30(hari)) { break; }
        console.log(hari + " November " + tahun)
        break;
    case 12:
        if (wrongInputDate31(hari)) { break; }
        console.log(hari + " Desember " + tahun)
        break;
}





