// Tugas 4 - Functions

// No. 1
console.log("No. 1")
function teriak() {
    console.log("Halo Sanbers!")
}
teriak()

// No. 2
console.log("\n")
console.log("No. 2")
function kalikan(num1, num2) {
    return num1 * num2
}

let num1 = 10
let num2 = 2
let hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// No. 3
console.log("\n")
console.log("No. 3")
function introduce(name, age, address, hobby) {
    let res = "Nama saya " + name + ", umur saya " + age + " tahun" + ", alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
    return res
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
