// A. Bandingkan Angka
console.log("A. Bandingkan Angka")
function bandingkan(num1, num2 = 0) {
    if (parseInt(num1) < 0 || parseInt(num2) < 0) {
        return -1
    } else if (parseInt(num1) < parseInt(num2)) {
        return parseInt(num2)
    } else if (parseInt(num1) > parseInt(num2)) {
        return parseInt(num1)
    } else {
        return -1
    }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


// B. Balik String
console.log("\n")
console.log("B. Balik String")
function balikString(aString) {
    let strReversed = ""
    for (let i = aString.length - 1; i >= 0; i--) {
        strReversed += aString[i]
    }
    return strReversed
}

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


// C. Palindrome
console.log("\n")
console.log("C. Palindrome")
function palindrome(aString) {
    let isPalindrome = balikString(aString)
    return aString === isPalindrome
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false