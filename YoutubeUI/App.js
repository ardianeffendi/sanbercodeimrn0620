/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Component from './Tugas/Tugas12/App';

const App = () => {
  return (<Component />);
}

export default App;
