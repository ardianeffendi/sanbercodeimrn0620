// Soal 1
class Score {
    constructor(subject, points, email) {
        this.email = email
        this.subject = subject
        this.points = points
    }

    average() {
        if (Array.isArray(this.points)) {
            let total, averageMark
            this.points.forEach(element => {
                total += element
            });
            return averageMark = total / this.points.length
        }
    }
}


// Soal 2
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    let arrData = []
    let i
    if (data[0].includes(subject)) {
        i = function () {
            for (let j = 0; j < data[0].length; j++) {
                if (data[0][j] === subject) {
                    return j
                }
            }
        }
        for (let index = 1; index < data.length; index++) {
            let score = new Score(subject, data[index][i], data[index[0]])
            arrData.push(score)
        }
    }
    return arrData
}



// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


// Soal 3
function recapScores(data) {
    //let person = new Score()
}