// No. 1 Looping While
function loopPertama() {
    let i = 2
    while (i <= 20) {
        console.log(i + " - " + "I love coding")
        i += 2
    }
}
console.log("LOOPING PERTAMA")
loopPertama()

function loopKedua() {
    let i = 20
    while (i >= 2) {
        console.log(i + " - " + "I will become a mobile developer")
        i -= 2
    }
}
console.log("LOOPING KEDUA")
loopKedua()


// No. 2 Looping menggunakan 
console.log("\n")
function forLoop() {
    for (let i = 1; i <= 20; i++) {
        if (i % 3 === 0 && i % 2 !== 0) {
            console.log(i + " - I Love Coding")
        } else if (i % 2 !== 0) {
            console.log(i + " - Santai")
        } else {
            console.log(i + " - Berkualitas")
        }
    }
}
console.log("Output")
forLoop()


// No.3 Membuat Persegi Panjang # 
console.log("\n")
function hashRectangle() {
    let i = 0
    while (i < 4) {
        let rectangle = ""
        for (let j = 0; j < 8; j++) {
            rectangle += "#"
        }
        console.log(rectangle)
        i++
    }
}

hashRectangle()


// No. 4 Membuat Tangga
console.log("\n")
function hashStaircase() {
    let i = 1
    while (i < 8) {
        let hash = ""
        for (let j = 0; j < i; j++) {
            hash += "#"
        }
        console.log(hash)
        i++
    }
}

hashStaircase()


// No. 5 Membuat Papan Catur
console.log("\n")
function forLoopRowOdd(row, aHash, aSpace) {
    for (let j = 0; j < 4; j++) {
        row = row + aSpace + aHash
    } return row
}

function forLoopRowEven(row, aHash, aSpace) {
    for (let j = 0; j < 4; j++) {
        row = row + aHash + aSpace
    } return row
}

function chessBoard() {
    let space = " "
    let hash = "#"
    let currentRow = ""
    let i = 1
    while (i <= 8) {
        if (i % 2 === 0) {
            console.log(forLoopRowEven(currentRow, hash, space))
        } else { console.log(forLoopRowOdd(currentRow, hash, space)) }
        i++
    }
}

chessBoard()