const title = (number, question) => console.log(`\n${number}. ${question}`)

title(1, "Mengubah Fungsi menjadi fungsi arrow")
// 1. Mengubah Fungsi menjadi fungsi arrow
// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }

//   golden()
const golden = () => console.log("this is golden!!")
golden()


title(2, "Sederhanakan menjadi Object literal di ES6")
// 2. Sederhanakan menjadi Object literal di ES6
// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()


title(3, "Destructuring")
// 3. Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject
// Driver code
console.log(firstName, lastName, destination, occupation)


title(4, "Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

const combined = [...west, ...east]
//Driver Code
console.log(combined)


title(5, "Template Literals")
const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'

let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 