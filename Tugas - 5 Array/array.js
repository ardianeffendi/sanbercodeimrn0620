// Tugas 5 - Array

// Soal 1 (Range)
console.log("=== Soal No. 1 (Range) ===")
function range(startNum, finishNum) {
    let array = []
    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            array.push(i)
        }
    } else if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i++) {
            array.push(i)
        }
    } else if (!startNum || !finishNum) {
        return -1
    }

    return array
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal 2 (Range with Step)
console.log("\n")
console.log("=== Soal No. 2 (Range with Step) ===")
function rangeWithStep(startNum, finishNum, step) {
    let array = []
    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i)
        }
    } else if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i)
        }
    } else if (!startNum || !finishNum) {
        return -1
    }

    return array
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal 3 (Sum of Range)
console.log("\n")
console.log("=== Soal No. 3 (Sum of Range) ===")
function sum(firstNum, lastNum, step) {
    if (!firstNum && !lastNum && !step) {
        return 0
    } else if (!lastNum && !step) {
        return firstNum
    } else if (!step) {
        step = 1
    }
    const array = rangeWithStep(firstNum, lastNum, step)
    const result = (array.length / 2) * (array[0] + array[array.length - 1])
    return result
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal 4 (Array Multidimensi)
console.log("\n")
console.log("=== Soal No. 4 (Array Multidimensi) ===")
function dataHandling(dataArray) {
    const arrLength = dataArray.length
    let anArray = []
    let output = ""
    for (let i = 0; i < arrLength; i++) {
        anArray.push([`Nomor ID: ${dataArray[i][0]}
        Nama Lengkap: ${dataArray[i][1]}
        TTL: ${dataArray[i][2]}
        Hobi: ${dataArray[i][3]}\n`])
    }
    output = anArray.join("\n")
    return output
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input))


// Soal 5 (Balik Kata)
console.log("\n")
console.log("=== Soal No. 5 (Balik Kata) ===")
function balikKata(aString) {
    let strLength = aString.length - 1
    let reverseStrOutput = ""
    for (let i = strLength; i >= 0; i--) {
        reverseStrOutput += aString[i]
    }
    return reverseStrOutput
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// Soal 6 (Metode Array)
console.log("\n")
console.log("=== Soal No. 6 (Metode Array) ===")
function dataHandling2(anArray) {
    let newName = "Roman Alamsyah Elsharawy"
    let gender = "Pria"
    let school = "SMA Internasional Metro"
    anArray.splice(1, 2, newName, "Provinsi Bandar Lampung")
    anArray.splice(4, 1, gender, school)
    console.log(anArray)

    let date = anArray[3].split("/")
    function switchCaseMonth(dateArray) {
        let month = ""
        switch (parseInt(dateArray[1])) {
            case 1:
                month = "Januari"
                break;
            case 2:
                month = "Februari"
                break;
            case 3:
                month = "Maret"
                break;
            case 4:
                month = "April"
                break;
            case 5:
                month = "Mei"
                break;
            case 6:
                month = "Juni"
                break;
            case 7:
                month = "Juli"
                break;
            case 8:
                month = "Agustus"
                break;
            case 9:
                month = "September"
                break;
            case 10:
                month = "October"
                break;
            case 11:
                month = "November"
                break;
            case 12:
                month = "Desember"
                break;
        }
        return month
    }

    console.log(switchCaseMonth(date))
    console.log(date.sort((val1, val2) => parseInt(val1) - parseInt(val2)).reverse())
    console.log(anArray[3])
    let stopIndex = (anArray[1].indexOf("Elsharawy"))
    let name = function (anArray, stop) {
        let constructor = ""
        for (let i = 0; i < stop; i++) {
            constructor += anArray[i]
        }
        return constructor
    }
    console.log(name(anArray[1], stopIndex))

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);