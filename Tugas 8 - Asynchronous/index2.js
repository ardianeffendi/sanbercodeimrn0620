var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'OPM', timeSpent: 2000 }
]

function readBookPromise(index, time) {
    readBooksPromise(time, books[index])
        .then(timeLeft => {
            index++
            return readBookPromise(index, timeLeft)
        })
        .catch(noTime => { return noTime })
}

readBookPromise(0, 10000)