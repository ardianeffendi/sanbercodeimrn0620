var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function readBook(index, time) {
    if (index < books.length) {
        readBooks(time, books[index], timeLeft => {
            index++
            return readBook(index, timeLeft)
        })
    } else {
        console.log(`Sisa waktu: ${time}`)
    }
}

readBook(0, 10000)