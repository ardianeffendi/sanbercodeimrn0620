import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import skillData from '../Tugas14/skillData.json'
import ListItem from './ListItem'

const App = () => {
    const [items, setItems] = useState(skillData.items)

    return (
        <View style={styles.container}>
            <FlatList
                data={items}
                renderItem={({ item }) => <ListItem item={item} />}
            ></FlatList>

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    header: {
        position: 'absolute',
        height: 155,
        backgroundColor: 'white',
    },
    headerLogo: {
        position: 'absolute',
        height: 51,
        left: 187,
    },
    vectorHeader: {
        position: 'absolute',
        left: 16,
        top: 55,
        width: 32,
        height: 32,
    },
    textHeader: {
        position: 'absolute',
        height: 19,
        left: 56,
        top: 68,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 16,
    },
    skillHeader: {
        position: 'absolute',
        height: 42,
        left: 16,
        top: 103,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 36,
        lineHeight: 42,
    },
    textNavBar: {
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 12,
        lineHeight: 14,
    },

})

export default App;