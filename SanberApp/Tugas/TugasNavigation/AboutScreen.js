/* eslint-disable prettier/prettier */
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class AboutScreen extends Component {
    render() {
        return (
            <View style={style.container}>
                <Text style={style.textTtg}>Tentang Saya</Text>
                <View style={style.circle}></View>
                <Icon name="person" style={style.person} size={150}></Icon>


            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 8
    },
    textTtg: {
        position: "absolute",
        height: 42,
        left: 78,
        top: 64,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 36,
        lineHeight: 42,
        color: '#003366'
    },
    circle: {
        position: "absolute",
        width: 200,
        height: 200,
        left: 88,
        top: 118,
        borderRadius: 200 / 2,
        backgroundColor: '#EFEFEF'
    },
    person: {
        position: "absolute",
        left: 110,
        width: 156,
        top: 140,
        height: 156,
        color: '#CACACA'
    }
})