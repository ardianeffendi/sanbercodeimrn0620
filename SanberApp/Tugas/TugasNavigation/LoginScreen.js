/* eslint-disable prettier/prettier */
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={style.container}>
                <Image source={require('./assets/logo.png')} style={style.logoHeader} />
                <Text style={style.textLogin}>Login</Text>

                <Text style={style.textBodyUsername}>Username/Email</Text>
                <TextInput style={style.inputBoxUsername}></TextInput>
                <Text style={style.textBodyPassword}>Password</Text>
                <TextInput style={style.inputBoxPassword}></TextInput>

                <View style={style.boxMasuk} />
                <Text style={style.textMasuk}>Masuk</Text>
                <Text style={style.textAtau}>atau</Text>
                <View style={style.boxDaftar} />
                <Text style={style.textDaftar}>Daftar ?</Text>

            </View>
        )
    }
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 8
    },
    logoHeader: {
        position: "absolute",
        paddingLeft: 20,
        width: 375,
        height: 102,
        left: 0,
        top: 63
    },
    textLogin: {
        position: "absolute",
        top: 235,
        width: 88,
        height: 28,
        left: 143,
        textAlign: 'center',
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        color: '#003366',
        fontSize: 24,
        lineHeight: 28
    },
    textBodyUsername: {
        position: "absolute",

        height: 19,
        left: 40,
        top: 303,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    textBodyPassword: {
        position: "absolute",
        height: 19,
        left: 41,
        top: 390,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    inputBoxUsername: {
        position: 'absolute',
        width: 294,
        height: 48,
        left: 41,
        top: 326,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#003366'
    },
    inputBoxPassword: {
        position: 'absolute',
        width: 294,
        height: 48,
        left: 41,
        top: 413,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#003366'
    },
    textMasuk: {
        position: 'absolute',
        height: 28,
        left: 152,
        top: 499,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#FFFFFF'
    },
    boxMasuk: {
        position: "absolute",
        width: 140,
        height: 40,
        left: 118,
        top: 493,
        backgroundColor: '#3EC6FF',
        borderRadius: 16
    },
    textAtau: {
        position: 'absolute',
        height: 28,
        left: 164,
        top: 549,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#3EC6FF'
    },
    boxDaftar: {
        position: "absolute",
        width: 140,
        height: 40,
        left: 118,
        top: 593,
        backgroundColor: '#003366',
        borderRadius: 16
    },
    textDaftar: {
        position: 'absolute',
        height: 28,
        left: 146,
        top: 599,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#FFFFFF'
    },
})
