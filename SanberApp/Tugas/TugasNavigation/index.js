import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';

const AuthStack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <AuthStack.Screen name='LoginScreen' component={LoginScreen} />
    </NavigationContainer>
)
