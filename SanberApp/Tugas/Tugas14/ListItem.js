import React, { useState } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import skillData from './skillData.json'

const ListItem = ({ item }) => {
    return (
        <View style={[styles.container, styles.boxShadow]}>
            <Icon style={styles.skillLogo} name={item.iconName} size={80} />
            <Text style={styles.textList}>{item.skillName}</Text>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 2,
        borderColor: 'black',
        marginBottom: 8,
        backgroundColor: '#B4E9FF',
        borderRadius: 8,

    },
    boxShadow: Platform.OS === 'ios' ? {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
    } : { elevation: 8 },

    rectList: {
        position: 'absolute',
        width: 343,
        height: 129,
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
    },
    arrowList: {
        position: 'absolute',
        height: 80,
        width: 80,
        left: 286,
    },
    skillLogo: {
        height: 80,
        width: 80,
        left: 2,
    },
    textList: {
        position: 'absolute',
        marginLeft: '30%',
        marginTop: 5,
        fontFamily: 'roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28,
    }
})

export default ListItem;